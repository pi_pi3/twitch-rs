use std::str::FromStr;
use std::thread::{self, JoinHandle};
use std::sync::mpsc::{self, Receiver, Sender};

use reqwest::Client;
use ::irc::error::IrcError;
use ::irc::client::{IrcClient, Client as IrcClientTrait};
use ::irc::proto::message::Message;
use ::irc::proto::command::Command;
use log::{error, info};

use crate::{Bot, OAuth};
use crate::error::{Error, InvalidUserType, InvalidBroadcasterType, NoUsername};
use crate::irc;
use crate::sys;

#[derive(Debug)]
pub struct TwitchClient<T> {
    client: Client,
    irc_thread: JoinHandle<Result<(), Error>>,
    message_rx: Option<Receiver<Message>>,
    response_tx: Sender<Message>,
    bot: Option<T>,
    channel: String,
    prefix: String,
    queued: Option<Vec<Message>>,
}

impl<T: Bot> TwitchClient<T> {
    pub fn new(bot: T) -> Self {
        let client = Client::new();
        let (message_tx, message_rx) = mpsc::channel::<Message>();
        let (response_tx, response_rx) = mpsc::channel::<Message>();
        let name = bot.username().to_string();
        let channel = format!("#{}", bot.channel_name());
        let mychannel = channel.clone();
        let oauth = bot.oauth().to_string();
        let prefix = format!("{0}!{0}@{0}.tmi.twitch.tv", name);
        let irc_thread = thread::spawn(move || {
            let mut tx = message_tx;
            let mut rx = response_rx;
            let oauth = OAuth::from(&*oauth);
            let prefix = format!("{0}!{0}@{0}.tmi.twitch.tv", name);
            irc::connect_with(&name, oauth, &channel, move |client, message| {
                fn message_loop(
                    client: &IrcClient,
                    tx: &mut Sender<Message>,
                    rx: &mut Receiver<Message>,
                    message: Message,
                ) -> Result<(), IrcError> {
                    tx.send(message).expect("couldn't send PRIVMSG");
                    loop {
                        let response = rx.recv().expect("couldn't recv response to PRIVMSG");
                        match response.command {
                            Command::Raw(ref rpl, _, _) if rpl == "NIL" => break,
                            _ => {
                                info!("< {}", response);
                                client.send(response)?;
                            }
                        }
                    }
                    Ok(())
                }
                info!("> {}", message.to_string());
                match message.command {
                    Command::PING(_, _) => {
                        let response = Message::new(Some(&prefix), "PONG", vec![""], None).expect("invalid PONG");
                        info!("< {}", response);
                        client.send(response)?;
                        message_loop(client, &mut tx, &mut rx, message)?;
                    }
                    Command::PONG(_, _)
                    | Command::CONNECT(_, _, _)
                    | Command::JOIN(_, _, _)
                    | Command::PART(_, _)
                    | Command::PRIVMSG(_, _) => message_loop(client, &mut tx, &mut rx, message)?,
                    Command::Raw(ref rpl, _, _) if rpl == "WHISPER" => message_loop(client, &mut tx, &mut rx, message)?,
                    _ => {}
                }
                Ok(())
            })?
            .run()?;
            Ok(())
        });
        TwitchClient {
            client,
            irc_thread,
            message_rx: Some(message_rx),
            response_tx,
            bot: Some(bot),
            channel: mychannel,
            prefix,
            queued: None,
        }
    }

    pub fn say(&mut self, msg: &str) -> Result<(), Error> {
        for line in msg.lines() {
            let msg = Message {
                tags: None,
                prefix: Some(self.prefix.clone()),
                command: Command::PRIVMSG(self.channel.clone(), format!("{}", line)),
            };
            self.response_tx.send(msg.clone()).expect("couldn't send response");
            if let Some(ref mut queued) = self.queued {
                queued.push(msg);
            } else {
                self.queued = Some(vec![msg])
            }
        }
        Ok(())
    }

    pub fn action(&mut self, action: &str) -> Result<(), Error> {
        self.response_tx
            .send(Message {
                tags: None,
                prefix: Some(self.prefix.clone()),
                command: Command::PRIVMSG(self.channel.clone(), format!(":/me {}", action)),
            })
            .expect("couldn't send response");
        Ok(())
    }

    pub fn whisper(&mut self, username: &str, msg: &str) -> Result<(), Error> {
        for line in msg.lines() {
            let msg = Message {
                tags: None,
                prefix: Some(self.prefix.clone()),
                command: Command::PRIVMSG(self.channel.clone(), format!(":/w {} {}", username, line)),
            };
            self.response_tx.send(msg).expect("couldn't send response");
        }
        Ok(())
    }

    pub fn run(mut self) -> Result<(), Error> {
        let rx = self.message_rx.take().unwrap();
        let mut bot = self.bot.take().unwrap();
        loop {
            if let Some(queue) = self.queued.take() {
                for msg in queue {
                    if let Err(err) = self.process(&mut bot, msg, true) {
                        error!("{}", err);
                        self.say("Sorry, I didn't get that")?;
                    }
                }
            } else {
                let msg = rx.recv().expect("couldn't recv message");
                let result = self.process(&mut bot, msg, false);
                if let Err(err) = result {
                    error!("{}", err);
                    self.say("Sorry, I didn't get that")?;
                }
            }
        }
    }

    fn process(&mut self, bot: &mut T, message: Message, this: bool) -> Result<(), Error> {
        let username = message
            .prefix
            .clone()
            .and_then(|prefix| prefix.split("!").next().map(ToString::to_string))
            .ok_or_else(|| Error::from(NoUsername));
        let userstate = username.and_then(|username| {
            let mut helix_state: sys::UserstateQuery = sys::query(
                &self.client,
                bot.oauth(),
                bot.client_id(),
                &format!("/helix/users?login={}", username),
            )?;

            Userstate::try_from_sys(helix_state.data.remove(0))
        });
        let username = message
            .prefix
            .and_then(|prefix| prefix.split("!").next().map(ToString::to_string))
            .ok_or_else(|| Error::from(NoUsername));
        match message.command {
            Command::PRIVMSG(_, msg) => {
                let userstate = userstate?;
                let mut has_bits = false;
                if let Some(ref tags) = message.tags {
                    for tag in tags {
                        if tag.0 == "bits" {
                            has_bits = true;
                            break;
                        }
                    }
                }
                if has_bits {
                    bot.on_message(self, userstate.clone(), &msg, this)?;
                    bot.on_cheer(self, userstate, &msg, this)?;
                } else if msg.starts_with("\u{1}ACTION") && msg.ends_with('\u{1}') {
                    bot.on_message(self, userstate.clone(), &format!("/me {}", msg), this)?;
                    bot.on_action(self, userstate, &msg[7..msg.len() - 1], this)?;
                } else {
                    bot.on_message(self, userstate.clone(), &msg, this)?;
                    bot.on_chat(self, userstate, &msg, this)?;
                }
            }
            Command::Raw(ref rpl, _, ref msg) if rpl == "WHISPER" => {
                let userstate = userstate?;
                let msg = msg.as_ref().unwrap();
                bot.on_message(self, userstate.clone(), &format!("/w {} {}", bot.username(), msg), this)?;
                bot.on_whisper(self, &username?, userstate, msg)?;
            }
            Command::CONNECT(server, port, _) => bot.on_connected(
                self,
                &server,
                u16::from_str(&port).expect(&format!("invalid port: {}", port)),
            )?,
            Command::JOIN(_, _, _) => bot.on_join(self, &username?)?,
            Command::PART(_, _) => bot.on_part(self, &username?)?,
            Command::PING(_, _) => bot.on_ping(self)?,
            Command::PONG(_, _) => bot.on_pong(self)?,
            _ => {}
        }
        self.response_tx
            .send(Message::new(Some(&self.prefix), "NIL", vec![], None)?)
            .expect("couldn't send response");
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub enum MessageType {
    Action,
    Chat,
    Whisper,
    Other(String),
}

impl Default for MessageType {
    fn default() -> Self {
        MessageType::Other("<unknown>".to_string())
    }
}

#[derive(Debug, Default, Clone)]
pub struct Userstate {
    pub id: String,
    pub login: String,
    pub display_name: String,
    pub email: Option<String>,
    pub description: String,
    pub ty: UserType,
    pub broadcaster_type: BroadcasterType,
    pub offline_image_url: String,
    pub profile_image_url: String,
    pub view_count: usize,
}

impl Userstate {
    pub(crate) fn try_from_sys(userstate: sys::Userstate) -> Result<Self, Error> {
        let userstate = Userstate {
            id: userstate.id,
            login: userstate.login,
            display_name: userstate.display_name,
            email: userstate.email,
            description: userstate.description,
            ty: FromStr::from_str(&userstate.ty)?,
            broadcaster_type: FromStr::from_str(&userstate.broadcaster_type)?,
            offline_image_url: userstate.offline_image_url,
            profile_image_url: userstate.profile_image_url,
            view_count: userstate.view_count,
        };
        Ok(userstate)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum UserType {
    Normal,
    GlobalMod,
    Admin,
    Staff,
}

impl Default for UserType {
    fn default() -> Self {
        UserType::Normal
    }
}

impl FromStr for UserType {
    type Err = InvalidUserType;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "" => Ok(UserType::Normal),
            "global_mod" => Ok(UserType::GlobalMod),
            "admin" => Ok(UserType::Admin),
            "staff" => Ok(UserType::Staff),
            _ => Err(InvalidUserType(string.to_string())),
        }
    }
}

#[derive(Debug, Clone)]
pub enum BroadcasterType {
    Normal,
    Affiliate,
    Partner,
}

impl Default for BroadcasterType {
    fn default() -> Self {
        BroadcasterType::Normal
    }
}

impl FromStr for BroadcasterType {
    type Err = InvalidBroadcasterType;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "" => Ok(BroadcasterType::Normal),
            "affiliate" => Ok(BroadcasterType::Affiliate),
            "partner" => Ok(BroadcasterType::Partner),
            _ => Err(InvalidBroadcasterType(string.to_string())),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Roomstate;

#[derive(Debug, Clone)]
pub struct Emote;

#[derive(Debug, Clone)]
pub enum MsgId {
    AlreadyBanned,
    AlreadyEmoteOnlyOn,
    AlreadyEmoteOnlyOff,
    AlreadySubsOn,
    AlreadySubsOff,
    BadBanAdmin,
    BadBanBroadcaster,
    BadBanGlobalMod,
    BadBanSelf,
    BadBanStaff,
    BadCommercialError,
    BadHostHosting,
    BadHostRateExceeded,
    BadModMod,
    BadModBanned,
    BadTimeoutAdmin,
    BadTimeoutGlobalMod,
    BadTimeoutSelf,
    BadTimeoutStaff,
    BadUnbanNoBan,
    BadUnmodMod,
    BanSuccess,
    CmdsAvailable,
    ColorChanged,
    CommercialSuccess,
    EmoteOnlyOn,
    EmoteOnlyOff,
    HostsRemaining,
    HostTargetWentOffline,
    ModSuccess,
    MsgBanned,
    MsgCensoredBroadcaster,
    MsgChannelSuspended,
    MsgDuplicate,
    MsgEmoteonly,
    MsgRatelimit,
    MsgSubsonly,
    MsgTimedout,
    MsgVerifiedEmail,
    NoHelp,
    NoPermission,
    NotHosting,
    TimeoutSuccess,
    UnbanSuccess,
    UnmodSuccess,
    UnrecognizedCmd,
    UsageBan,
    UsageClear,
    UsageColor,
    UsageCommercial,
    UsageDisconnect,
    UsageEmoteOnlyOn,
    UsageEmoteOnlyOff,
    UsageHelp,
    UsageHost,
    UsageMe,
    UsageMod,
    UsageMods,
    UsageR9kOn,
    UsageR9kOff,
    UsageSlowOn,
    UsageSlowOff,
    UsageSubsOn,
    UsageSubsOff,
    UsageTimeout,
    UsageUnban,
    UsageUnhost,
    UsageUnmod,
    WhisperInvalidSelf,
    WhisperLimitPerMin,
    WhisperLimitPerSec,
    WhisperRestrictedRecipient,
    Other(String),
}
