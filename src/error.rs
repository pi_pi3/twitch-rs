use std::fmt::{self, Display};
use std::string::FromUtf8Error;
use std::error::Error as StdError;

#[derive(Debug)]
pub struct InvalidUserType(pub String);

impl Display for InvalidUserType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid type: {}", self.0)
    }
}

#[derive(Debug)]
pub struct InvalidBroadcasterType(pub String);

impl Display for InvalidBroadcasterType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid broadcaster_type: {}", self.0)
    }
}

#[derive(Debug)]
pub struct NoUsername;

impl Display for NoUsername {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "no username in this request")
    }
}

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    Json(serde_json::Error),
    Utf8(FromUtf8Error),
    Irc(irc::error::IrcError),
    IrcMessage(irc::error::MessageParseError),
    UserType(InvalidUserType),
    BroadcasterType(InvalidBroadcasterType),
    NoUsername(NoUsername),
    User(Box<StdError + Send>),
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Error {
        Error::Reqwest(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Error {
        Error::Json(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Error {
        Error::Utf8(err)
    }
}

impl From<irc::error::IrcError> for Error {
    fn from(err: irc::error::IrcError) -> Error {
        Error::Irc(err)
    }
}

impl From<irc::error::MessageParseError> for Error {
    fn from(err: irc::error::MessageParseError) -> Error {
        Error::IrcMessage(err)
    }
}

impl From<InvalidUserType> for Error {
    fn from(err: InvalidUserType) -> Error {
        Error::UserType(err)
    }
}

impl From<InvalidBroadcasterType> for Error {
    fn from(err: InvalidBroadcasterType) -> Error {
        Error::BroadcasterType(err)
    }
}

impl From<NoUsername> for Error {
    fn from(err: NoUsername) -> Error {
        Error::NoUsername(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Reqwest(err) => Display::fmt(err, f),
            Error::Json(err) => Display::fmt(err, f),
            Error::Utf8(err) => Display::fmt(err, f),
            Error::Irc(err) => Display::fmt(err, f),
            Error::IrcMessage(err) => Display::fmt(err, f),
            Error::UserType(err) => Display::fmt(err, f),
            Error::BroadcasterType(err) => Display::fmt(err, f),
            Error::NoUsername(err) => Display::fmt(err, f),
            Error::User(err) => Display::fmt(err, f),
        }
    }
}
