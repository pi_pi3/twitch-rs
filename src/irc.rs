use irc::client::{IrcClient, Client, ext::ClientExt, reactor::IrcReactor};
use irc::proto::{message::Message, command::{Command, CapSubCommand}};
use irc::client::data::config::Config;
use futures::IntoFuture;
use log::info;

use crate::OAuth;
use crate::error::Error;

const IRC_DOMAIN: &str = "irc.chat.twitch.tv";
const IRC_PORT: u16 = 6667;

pub fn connect_with<F, U>(name: &str, pass: OAuth, channel: &str, f: F) -> Result<IrcReactor, Error>
where
    F: FnMut(&IrcClient, Message) -> U + 'static,
    U: IntoFuture<Item = (), Error = irc::error::IrcError> + 'static,
{
    info!(
        "connecting to irc://{}:{}: {}: @{}",
        IRC_DOMAIN, IRC_PORT, channel, name
    );
    let config = Config {
        nickname: Some(name.to_string()),
        username: Some(name.to_string()),
        realname: Some(name.to_string()),
        password: Some(format!("oauth:{}", pass)),
        server: Some(IRC_DOMAIN.to_string()),
        port: Some(IRC_PORT),
        use_ssl: Some(false),
        channels: Some(vec![channel.to_string()]),
        ..Default::default()
    };
    let client = IrcClient::from_config(config)?;
    client.identify()?;
    client.send(Command::CAP(
        None,
        CapSubCommand::REQ,
        Some("twitch.tv/membership".to_string()),
        None,
    ))?;
    client.send(Command::CAP(
        None,
        CapSubCommand::REQ,
        Some("twitch.tv/tags".to_string()),
        None,
    ))?;
    client.send(Command::CAP(
        None,
        CapSubCommand::REQ,
        Some("twitch.tv/commands".to_string()),
        None,
    ))?;
    let mut reactor = IrcReactor::new()?;
    reactor.register_client_with_handler(client, f);
    Ok(reactor)
}
