#[macro_use]
extern crate serde_derive;

use std::fmt::{self, Display};

pub use error::Error;
pub use api::{TwitchClient, Userstate, Roomstate, Emote, MsgId};

pub mod error;
pub mod irc;
pub mod api;
mod sys;

#[derive(Debug, Clone, Copy)]
pub struct OAuth<'a>(pub &'a str);

impl<'a> Display for OAuth<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self.0, f)
    }
}

impl<'a> From<&'a str> for OAuth<'a> {
    fn from(string: &'a str) -> Self {
        if string.starts_with("oauth:") {
            OAuth(&string[6..])
        } else {
            OAuth(string)
        }
    }
}

pub trait Bot: Sized {
    fn username(&self) -> &str;
    fn channel_name(&self) -> &str;
    fn oauth(&self) -> OAuth<'_>;
    fn client_id(&self) -> &str;

    fn on_ping(&mut self, _client: &mut TwitchClient<Self>) -> Result<(), Error> {
        Ok(())
    }

    fn on_pong(&mut self, _client: &mut TwitchClient<Self>) -> Result<(), Error> {
        Ok(())
    }

    fn on_action(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _userstate: Userstate,
        _msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_ban(&mut self, _client: &mut TwitchClient<Self>, _user: &str, _reason: &str) -> Result<(), Error> {
        Ok(())
    }

    fn on_chat(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _userstate: Userstate,
        _msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_cheer(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _userstate: Userstate,
        _msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_connected(&mut self, _client: &mut TwitchClient<Self>, _addr: &str, _port: u16) -> Result<(), Error> {
        Ok(())
    }

    fn on_disconnected(&mut self, _client: &mut TwitchClient<Self>, _reason: &str) -> Result<(), Error> {
        Ok(())
    }

    fn on_join(&mut self, _client: &mut TwitchClient<Self>, _username: &str) -> Result<(), Error> {
        Ok(())
    }

    fn on_part(&mut self, _client: &mut TwitchClient<Self>, _username: &str) -> Result<(), Error> {
        Ok(())
    }

    fn on_resub(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _username: &str,
        _months: usize,
        _msg: &str,
        _userstate: Userstate,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_subscription(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _username: &str,
        _msg: &str,
        _userstate: Userstate,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_whisper(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _username: &str,
        _userstate: Userstate,
        _msg: &str,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn on_message(
        &mut self,
        _client: &mut TwitchClient<Self>,
        _userstate: Userstate,
        _msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        Ok(())
    }
}
