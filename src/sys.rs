use serde::de::DeserializeOwned;
use reqwest::Client;
use log::info;

use crate::error::Error;
use crate::OAuth;

#[derive(Debug, Clone, Deserialize)]
pub struct Userstate {
    pub id: String,
    pub login: String,
    pub display_name: String,
    pub email: Option<String>,
    pub description: String,
    #[serde(rename = "type")]
    pub ty: String,
    pub broadcaster_type: String,
    pub offline_image_url: String,
    pub profile_image_url: String,
    pub view_count: usize,
}

#[derive(Debug, Clone, Deserialize)]
pub struct UserstateQuery {
    pub data: Vec<Userstate>,
}

pub fn query<'a, T: DeserializeOwned>(
    client: &Client,
    oauth: OAuth<'a>,
    client_id: &str,
    url: &str,
) -> Result<T, Error> {
    let url = format!("https://api.twitch.tv{}", url);
    info!("GET {}", url);
    client
        .get(&url)
        .header("Authorization", format!("Bearer {}", oauth))
        .header("Client-Id", client_id)
        .send()
        .and_then(|mut response| response.json())
        .map_err(From::from)
}
