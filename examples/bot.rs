use twitch::{Bot as TwitchBot, TwitchClient, Userstate, OAuth, Error};

#[derive(Debug, Clone, Copy)]
struct Bot;

impl TwitchBot for Bot {
    fn channel_name(&self) -> &str {
        "walterpi"
    }

    fn username(&self) -> &str {
        "walterpot"
    }

    fn oauth(&self) -> OAuth<'_> {
        OAuth::from(include_str!("../secret.oauth"))
    }

    fn client_id(&self) -> &str {
        include_str!("../secret.client-id")
    }

    fn on_chat(
        &mut self,
        client: &mut TwitchClient<Self>,
        userstate: Userstate,
        msg: &str,
        this: bool,
    ) -> Result<(), Error> {
        if !this {
            if msg.trim().starts_with(self.username()) {
                client.say(self, &format!("Hi, {}!", userstate.display_name))?;
            }
        }
        Ok(())
    }

    fn on_action(
        &mut self,
        client: &mut TwitchClient<Self>,
        userstate: Userstate,
        msg: &str,
        this: bool,
    ) -> Result<(), Error> {
        if !this {
            if msg.trim().starts_with("dabs") {
                client.say(self, &format!("{} dabbed", userstate.display_name))?;
            }
        }
        Ok(())
    }

    fn on_whisper(
        &mut self,
        client: &mut TwitchClient<Self>,
        username: &str,
        userstate: Userstate,
        msg: &str,
    ) -> Result<(), Error> {
        client.whisper(username, msg)
    }
}

fn main() -> Result<(), Error> {
    env_logger::init();
    let client = TwitchClient::new(Bot);
    client.run()
}
