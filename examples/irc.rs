use futures::{Stream, Future};
use ::irc::client::Client;
use ::irc::client::ext::ClientExt;
use ::irc::proto::message::Message;
use ::irc::proto::command::Command;

use twitch::irc;
use twitch::error::Error;

fn main() -> Result<(), Error> {
    let name = "walterpot";
    let channel = "#walterpi";
    let prefix = format!("{0}!{0}@{0}.tmi.twitch.tv", name);
    irc::connect_with(
        name,
        twitch::OAuth::from(include_str!("../secret.oauth")),
        channel,
        move |client, message| {
            print!("> {}", message);
            match message.command {
                Command::PING(server1, server2) => client.send_pong("")?,
                Command::PRIVMSG(_, ref msg) => {
                    if let Some(target) = message.response_target() {
                        let response =
                            Message::new(Some(&prefix), "PRIVMSG", vec![target, msg], None).expect("invalid message");
                        println!("< {}", response);
                        client.send(response)?;
                    }
                }
                _ => {}
            }
            Ok(())
        },
    )?
    .run()?;
    Ok(())
}
